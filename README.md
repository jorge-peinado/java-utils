# java-utilities
Some Java utils.

db_interlock.java:
This small Java program causes the infamous "ORA-00060: Deadlock detected." in Oracle databases.
Has been tested in Fedora 24 with OpenJDK Version 1.8.0.111 and Oracle 12c.
It has been tested with the following packages installed:

- oracle-instantclient12.1-jdbc-12.1.0.2.0-1.x86_64
- oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64

An Oracle environment is required, pointing to an TNSNAMES.ORA.
Both are included in the folder.

The idea behind this is to test the behaviour of other RDBMS (i.e.PostgreSQL) under the same conditions.
