import java.util.Scanner;
import java.util.concurrent.locks.*;
import java.lang.Object;
import java.sql.*;


/*****************************/
/* Main: class db_interlock. */
/*****************************/

public class db_interlock {
  public static final Lock lock1 = new ReentrantLock();
  public static final Lock lock2 = new ReentrantLock();
  public static final Condition condvar1 = lock1.newCondition();
  public static final Condition condvar2 = lock2.newCondition();

  public static void main(String[] args) {

    // Required for read a key.
    Scanner scanner = new Scanner(System.in);

    // Wait for pressing enter to start
    System.out.println("[0] Hello. Main thread started. Press enter to continue ...");
    scanner.nextLine();

    // Launch our two threads
    Runnable r1 = new Runnable1();
    Thread t1 = new Thread(r1);

    Runnable r2 = new Runnable2();
    Thread t2 = new Thread(r2);

    t1.start();
    t2.start();

    // Wait for pressing enter to signal in the condition variable 1.
    System.out.println("[0] Both threads should be wating in the condition variable 1. Press enter to signal using the variable ...");
    scanner.nextLine();

    try {
      lock1.lock();
      condvar1.signalAll();
    }
    finally {
      lock1.unlock();
    }

    // Wait for pressing enter to signal in the condition variable 2.
    System.out.println("[0] Now both threads should be wating in the condition variable 2. Press enter to signal using the variable ...");
    scanner.nextLine();

    try {
      lock2.lock();
      condvar2.signalAll();
    }
    finally {
      lock2.unlock();
    }

    System.out.println("[0] End. Press enter to finish ...");
    scanner.nextLine();
  }
}



/************/
/* Thread 1 */
/************/

class Runnable1 implements Runnable {
  public void run() {

    // JDBC driver name and database URL
    final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final String DB_URL = "jdbc:oracle:thin:@linux12c:1521:test";

    //  Database credentials
    final String USER = "scott";
    final String PASS = "tiger";

    // Connection descriptor and statement;
    Connection conn = null;
    Statement stmt = null;

    // SQL string
    String sql;

    try{
      Class.forName("oracle.jdbc.driver.OracleDriver");
      conn = DriverManager.getConnection(DB_URL,USER,PASS);
      conn.setAutoCommit(false);

      System.out.println("[1] Hello. Thread #1 started and connected to the database.");
      System.out.println("[1] I'm going to wait into the condition variable 1.");

      try {
        wait_in_cond1();
      } catch (Exception e) {
        System.out.println("[1] Got InterruptedException in condition 1.");
        e.printStackTrace();
      }
      System.out.println("[1] Woken up. Insert and a commit in table 1 ...");

      stmt = conn.createStatement();
      sql = "insert into scott.table1 values(1,'Thread #1','Initial value by #1','Initial -- #1')";
      stmt.executeUpdate(sql);
      stmt = conn.createStatement();
      sql = "insert into scott.table1 values(2,'Thread #1','Initial value by #1','Initial -- #1')";
      stmt.executeUpdate(sql);
      conn.commit();

      System.out.println("[1] Done. Now update in table 1 with row exclusive lock. No commit yet.");

      stmt = conn.createStatement();
      sql = "lock table scott.table1 in row exclusive mode";
      stmt.executeUpdate(sql);

      stmt = conn.createStatement();
      sql = "update scott.table1 set content = 'Updated by #1', last_updater = 'Update -- #1' where row_number = 2";
      stmt.executeUpdate(sql);

      System.out.println("[1] Done. Go to wait in the condition variable 2.");

      try {
        wait_in_cond2();
      } catch (Exception e) {
        System.out.println("[1] Got InterruptedException in condition 2.");
        e.printStackTrace();
      }
      System.out.println("[1] Woken up. Here I go. Try to update table 2 ...");

      stmt = conn.createStatement();
      sql = "update scott.table2 set content = 'Updated by #1', last_updater = 'Update -- #1' where row_number = 2";
      stmt.executeUpdate(sql);

      System.out.println("[1] After update. Try to commit ...");
      conn.commit();
      System.out.println("[1] Commited.");

      // Clean up environment
      stmt.close();
      conn.close();

    } catch (SQLException se) {
      //Handle errors for JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      } catch (SQLException se2) {
      }// nothing we can do
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
    }//end try

    System.out.println("[1] All cleaned up. Goodbye!!.");
    return;
  }

  private void wait_in_cond1() throws InterruptedException {
    try {
      db_interlock.lock1.lock();
      db_interlock.condvar1.await();
    }
    finally {
      db_interlock.lock1.unlock();
    }
  }

  private void wait_in_cond2() throws InterruptedException {
    try {
      db_interlock.lock2.lock();
      db_interlock.condvar2.await();
    }
    finally {
      db_interlock.lock2.unlock();
    }
  }
}



/************/
/* Thread 2 */
/************/

class Runnable2 implements Runnable {
  public void run() {

    // JDBC driver name and database URL
    final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final String DB_URL = "jdbc:oracle:thin:@linux12c:1521:test";

    //  Database credentials
    final String USER = "scott";
    final String PASS = "tiger";

    // Connection descriptor and statement;
    Connection conn = null;
    Statement stmt = null;

    // SQL string
    String sql;

    try{
      Class.forName("oracle.jdbc.driver.OracleDriver");
      conn = DriverManager.getConnection(DB_URL,USER,PASS);
      conn.setAutoCommit(false);

      System.out.println("[2] Hello. Thread #2 started and connected to the database.");
      System.out.println("[2] I'm going to wait into the condition variable 1.");

      try {
        wait_in_cond1();
      } catch (Exception e) {
        System.out.println("[2] Got InterruptedException in condition 1.");
        e.printStackTrace();
      }
      System.out.println("[2] Woken up. Insert and a commit in table 2 ...");

      stmt = conn.createStatement();
      sql = "insert into scott.table2 values(1,'Thread #2','Initial value by #2','Initial -- #2')";
      stmt.executeUpdate(sql);
      stmt = conn.createStatement();
      sql = "insert into scott.table2 values(2,'Thread #2','Initial value by #2','Initial -- #2')";
      stmt.executeUpdate(sql);
      conn.commit();

      System.out.println("[2] Done. Now update in table 2 with row exclusive lock. No commit yet.");

      stmt = conn.createStatement();
      sql = "lock table scott.table2 in row exclusive mode";
      stmt.executeUpdate(sql);

      stmt = conn.createStatement();
      sql = "update scott.table2 set content = 'Updated by #1', last_updater = 'Update -- #1' where row_number = 2";
      stmt.executeUpdate(sql);

      System.out.println("[2] Done. Go to wait into the condition variable 2.");

      try {
        wait_in_cond2();
      } catch (Exception e) {
        System.out.println("[2] Got InterruptedException in condition 2.");
        e.printStackTrace();
      }
      System.out.println("[2] Woken up. Here I go. Try to update table 1 ...");

      stmt = conn.createStatement();
      sql = "update scott.table1 set content = 'Updated by #2', last_updater = 'Update -- #2' where row_number = 2";
      stmt.executeUpdate(sql);

      System.out.println("[2] After update. Try to commit ...");
      conn.commit();
      System.out.println("[2] Commited.");

      // Clean up environment
      stmt.close();
      conn.close();

    } catch (SQLException se) {
      //Handle errors for JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      } catch (SQLException se2) {
      }// nothing we can do
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
    }//end try

    System.out.println("[2] All cleaned up. Goodbye!!.");
    return;
  }

  private void wait_in_cond1() throws InterruptedException {
    try {
      db_interlock.lock1.lock();
      db_interlock.condvar1.await();
    }
    finally {
      db_interlock.lock1.unlock();
    }
  }

  private void wait_in_cond2() throws InterruptedException {
    try {
      db_interlock.lock2.lock();
      db_interlock.condvar2.await();
    }
    finally {
      db_interlock.lock2.unlock();
    }
  }
}


